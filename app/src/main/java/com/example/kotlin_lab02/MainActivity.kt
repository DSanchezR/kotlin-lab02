package com.example.kotlin_lab02

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {

            when (edtAnio.text.toString().toInt()) {
                in 1994..2010 -> {
                    tvGeneracion.text = getString(R.string.main_activity_generacion_z)
                    tvPoblacion.text = getString(R.string.main_activity_poblacion_generacion_x)
                    imgRasgo.setImageResource(R.drawable.generacion_z)
                }
                in 1981..1993 -> {
                    tvGeneracion.text = getString(R.string.main_activity_generacion_y)
                    tvPoblacion.text = getString(R.string.main_activity_poblacion_generacion_y)
                    imgRasgo.setImageResource(R.drawable.generacion_y)
                }
                in 1969..1980 -> {
                    tvGeneracion.text = getString(R.string.main_activity_generacion_x)
                    tvPoblacion.text = getString(R.string.main_activity_poblacion_generacion_x)
                    imgRasgo.setImageResource(R.drawable.generacion_x)
                }
                in 1949..1968 -> {
                    tvGeneracion.text = getString(R.string.main_activity_baby_boom)
                    tvPoblacion.text = getString(R.string.main_activity_poblacion_baby_boom)
                    imgRasgo.setImageResource(R.drawable.baby_boom)
                }
                in 1930..1948 -> {
                    tvGeneracion.text =getString(R.string.main_activity_silent_generation)
                    tvPoblacion.text = getString(R.string.main_activity_poblacion_silent_generation)
                    imgRasgo.setImageResource(R.drawable.silent_generation)
                }
            }

        }

    }
}